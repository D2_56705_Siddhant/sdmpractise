const express = require('express')
const app = express()

const cors = require('cors')

app.use(cors('*'))
app.use(express.json())

const routerEmployee = require('./routes/employee')

app.use('/employee', routerEmployee)

app.listen(4000, '0.0.0.0', () => {
    console.log('product-server started on port 4000')
})