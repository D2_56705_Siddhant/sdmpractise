const { request, response } = require('express')
const express = require('express')
const db = require('../db')
const utils = require('../utils')

const router = express.Router()


router.get('/', (request, response) => {
    const query = `SELECT * FROM Employee`

    db.execute(query, (error, result) => {
        response.send(utils.createResult(error,result))
    })
})

router.post('/', (request, response) => {
    
    const {empname, salary, age} = request.body

    const query = `INSERT INTO Employee (empname, salary, age) VALUES ('${empname}', ${salary}, ${age})`

    db.execute(query, (error, result) => {
        response.send(utils.createResult(error,result))
    })
})

router.put('/:id', (request, response) => {
    const {id} = request.params
    const {salary, age} = request.body

    const query = `UPDATE Employee SET salary = ${salary}, age = ${age} WHERE id = ${id}`

    db.execute(query, (error, result) => {
        response.send(utils.createResult(error,result))
    })

})

router.delete('/:id', (request, response) => {
    const {id} = request.params

    const query = `DELETE FROM Employee WHERE id = ${id}`

    db.execute(query, (error, result) => {
        response.send(utils.createResult(error,result))
    })
})

module.exports = router