CREATE TABLE Employee (
    id INTEGER PRIMARY KEY auto_increment,
    empname VARCHAR(25),
    salary INTEGER,
    age INTEGER
);


INSERT INTO Employee (empname, salary, age) VALUES ('Sam', 25000, 25);
INSERT INTO Employee (empname, salary, age) VALUES ('Dean', 35000, 29);
INSERT INTO Employee (empname, salary, age) VALUES ('Eileen', 29000, 23);
